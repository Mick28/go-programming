package main

// Forma de compilar este código para Linux
// GOOS=linux GOARCH=amd64 go build main.go

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Sistema Operativo: %v\nArquitectura: %v\n", runtime.GOOS, runtime.GOARCH)
}
